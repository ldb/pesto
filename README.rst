Pesto
=====

Pesto is a text-based human-editable and machine-transformable cooking recipe
interchange format.

For more information see the latest `rendered version`_ or its Literate
Haskell sources_.

.. _rendered version: https://6xq.net/pesto/
.. _sources: https://codeberg.org/ldb/pesto/src/branch/master/src/lib/Codec/Pesto.lhs

